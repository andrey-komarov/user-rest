package userrest.domain;

import java.util.HashMap;
import java.util.Map;

public class UserObjectMap {
    
    private Map<Long, UserObject> map = new HashMap<>();

    public Map<Long, UserObject> getMap() {
        return map;
    }

}
