package userrest.domain;

import java.util.Objects;

public class UserObject implements Comparable<UserObject> {

    private Long userId;
    private String name;
    private String surname;
    private int age;

    public Long getUserId() {
        return userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public UserObject() {}

    public UserObject(Long userId) {
        this.userId = userId;
    }

    @Override
    public int compareTo(UserObject o) {
        if (this.userId == null) {
            return o.userId == null ? 0 : -1;
        } else {
            if (o.userId == null) {
                return 1;
            } else {
                return this.userId.compareTo(o.userId);
            }
        }
    }

    public UserObject copy(Long userId) {
        UserObject clone = new UserObject(Objects.requireNonNull(userId));
        clone.name = this.name;
        clone.surname = this.surname;
        clone.age = this.age;
        return clone;
    }

}
