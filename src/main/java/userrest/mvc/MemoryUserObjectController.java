package userrest.mvc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import userrest.service.UserObjectService;

@RestController
@RequestMapping("/mem")
public class MemoryUserObjectController extends UserObjectController {

    @Autowired
    @Qualifier("memory")
    @Override
    public void setUserObjectService(UserObjectService userObjectService) {
        super.setUserObjectService(userObjectService);
    }

}





