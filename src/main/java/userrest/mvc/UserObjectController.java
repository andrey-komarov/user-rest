package userrest.mvc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import userrest.domain.UserObject;
import userrest.domain.UserObjectMap;
import userrest.service.UserObjectService;

public class UserObjectController {

    protected UserObjectService userObjectService;

    public UserObjectService getUserObjectService() {
        return userObjectService;
    }

    public void setUserObjectService(UserObjectService userObjectService) {
        this.userObjectService = userObjectService;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public UserObjectMap getAll() {
        return getUserObjectService().getAll();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public UserObject findById(@PathVariable("id") Long id) {
        return getUserObjectService().findById(id);
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public UserObject findById(@RequestBody UserObject userObject) {
        return getUserObjectService().save(userObject);
    }

}
