package userrest.mvc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import userrest.service.UserObjectService;

@RestController
@RequestMapping("/db")
public class DatabaseUserObjectController extends UserObjectController {

    @Autowired
    @Qualifier("database")
    @Override
    public void setUserObjectService(UserObjectService userObjectService) {
        super.setUserObjectService(userObjectService);
    }

}





