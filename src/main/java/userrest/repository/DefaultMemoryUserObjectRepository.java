package userrest.repository;

import userrest.domain.UserObject;
import userrest.domain.UserObjectMap;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.atomic.AtomicLong;

public class DefaultMemoryUserObjectRepository implements UserObjectRepository {

    private final ConcurrentSkipListSet<UserObject> userObjects = new ConcurrentSkipListSet<>();
    private final ConcurrentHashMap<Long, UserObject> idToUserObject = new ConcurrentHashMap<>();
    private final AtomicLong idGenerator = new AtomicLong(1L);

    public UserObject save(UserObject userObject) {
        if (userObject == null) {
            return null;
        }
        UserObject saved = userObject.copy(idGenerator.getAndIncrement());
        userObjects.add(saved);
        idToUserObject.put(saved.getUserId(), saved);
        return saved;
    }

    public UserObjectMap getAll() {
        UserObjectMap result = new UserObjectMap();
        for (UserObject source : userObjects) {
            UserObject copy = source.copy(source.getUserId());
            result.getMap().put(copy.getUserId(), copy);
        }
        return result;
    }

    public UserObject findById(Long id) {
        UserObject found = idToUserObject.get(id);
        if (found == null) {
            return null;
        }
        return found.copy(found.getUserId());
    }

}
