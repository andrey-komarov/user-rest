package userrest.repository;

import userrest.domain.UserObject;
import userrest.domain.UserObjectMap;

public interface UserObjectRepository {
    UserObject save(UserObject userObject);
    UserObjectMap getAll();
    UserObject findById(Long id);
}
