package userrest.repository;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.transaction.annotation.Transactional;
import userrest.domain.UserObject;
import userrest.domain.UserObjectMap;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Transactional
public class DefaultDatabaseUserObjectRepository extends NamedParameterJdbcDaoSupport
        implements UserObjectRepository {

    public static final String USER_OBJECT_TBL = "user_object";
    public static final String NAME_FLD = "name";
    public static final String SURNAME_FLD = "surname";
    public static final String AGE_FLD = "age";
    public static final String USER_ID_FLD = "user_id";

    public static final String SQL_SELECT_ALL =
            String.format("select uo.%s,uo.%s,uo.%s,uo.%s from %s uo",
                    USER_ID_FLD, NAME_FLD, SURNAME_FLD, AGE_FLD, USER_OBJECT_TBL);

    public static final String SQL_SELECT_BY_ID = SQL_SELECT_ALL +
            String.format(" where uo.%s=:%s", USER_ID_FLD, USER_ID_FLD);

    private SimpleJdbcInsert insertUserObject;

    @Override
    protected void initTemplateConfig() {
        super.initTemplateConfig();
        insertUserObject = new SimpleJdbcInsert(getJdbcTemplate())
                .withTableName(USER_OBJECT_TBL)
                .usingColumns(NAME_FLD, SURNAME_FLD, AGE_FLD)
                .usingGeneratedKeyColumns(USER_ID_FLD);
    }

    @Override
    public UserObject save(UserObject userObject) {
        if (userObject == null) {
            return null;
        }
        Map<String, Object> params = new HashMap<>(3);
        params.put(NAME_FLD, userObject.getName());
        params.put(SURNAME_FLD, userObject.getSurname());
        params.put(AGE_FLD, userObject.getAge());
        Number newId = insertUserObject.executeAndReturnKey(params);
        return userObject.copy(newId.longValue());
    }

    @Transactional(readOnly = true)
    @Override
    public UserObjectMap getAll() {
        List<UserObject> userObjects = getJdbcTemplate()
                .query(SQL_SELECT_ALL, new UserObjectRowMapper());
        UserObjectMap result = new UserObjectMap();
        for (UserObject userObject : userObjects) {
            result.getMap().put(userObject.getUserId(), userObject);
        }
        return result;
    }

    @Transactional(readOnly = true)
    @Override
    public UserObject findById(Long id) {
        if (id == null) {
            return null;
        }
        Map<String, Long> params = Collections.singletonMap(USER_ID_FLD, id);
        List<UserObject> userObjects = getNamedParameterJdbcTemplate()
                .query(SQL_SELECT_BY_ID, params, new UserObjectRowMapper());
        if (userObjects.isEmpty()) {
            return null;
        }
        if (userObjects.size() == 1) {
            return userObjects.get(0);
        }
        throw  new IllegalStateException("At most one row should be returned");
    }

    private class UserObjectRowMapper implements RowMapper<UserObject> {

        @Override
        public UserObject mapRow(ResultSet rs, int rowNum) throws SQLException {
            UserObject row = new UserObject(rs.getLong(USER_ID_FLD));
            row.setName(rs.getString(NAME_FLD));
            row.setSurname(rs.getString(SURNAME_FLD));
            row.setAge(rs.getInt(AGE_FLD));
            return row;
        }

    }
}
