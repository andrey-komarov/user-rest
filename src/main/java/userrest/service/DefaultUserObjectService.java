package userrest.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import userrest.domain.UserObject;
import userrest.domain.UserObjectMap;
import userrest.repository.UserObjectRepository;

import java.util.Objects;

public class DefaultUserObjectService implements UserObjectService {

    private UserObjectRepository repository;

    public DefaultUserObjectService(UserObjectRepository repository) {
        this.repository = Objects.requireNonNull(repository);
    }

    @Override
    public UserObject save(UserObject userObject) {
        return repository.save(userObject);
    }

    @Override
    public UserObjectMap getAll() {
        return repository.getAll();
    }

    @Override
    public UserObject findById(Long id) {
        return repository.findById(id);
    }

}
