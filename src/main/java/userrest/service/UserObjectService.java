package userrest.service;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import userrest.domain.UserObject;
import userrest.domain.UserObjectMap;

public interface UserObjectService {
    UserObject save(UserObject userObject);
    UserObjectMap getAll();
    UserObject findById(Long id);
}
