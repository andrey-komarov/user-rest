package userrest.testhelper;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.init.DatabasePopulator;
import org.springframework.test.jdbc.JdbcTestUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.core.io.Resource;

import javax.sql.DataSource;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/test")
public class HelperController implements ApplicationContextAware {

    private final Logger logger = LoggerFactory.getLogger(HelperController.class);

    private ApplicationContext applicationContext;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @RequestMapping(value = "/scripts", method = RequestMethod.POST)
    public void executeScripts(@RequestBody String scriptNames) throws Exception {
        logger.info("Execute scripts");
        Resource[] scripts = applicationContext.getResources(scriptNames);
        for (Resource script : scripts) {
            if (script.exists()) {
                logger.info("Script: \"{}\"", script.getURI());
                JdbcTestUtils.executeSqlScript(jdbcTemplate, script, false);
            }
        }
    }


}
