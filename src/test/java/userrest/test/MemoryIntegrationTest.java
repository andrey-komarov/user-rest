package userrest.test;

public class MemoryIntegrationTest extends IntegrationTest {

    @Override
    protected String getServiceRoot() {
        return "/mem/";
    }

}
