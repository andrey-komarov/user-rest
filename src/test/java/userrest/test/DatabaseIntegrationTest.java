package userrest.test;

import org.jboss.arquillian.container.test.api.RunAsClient;
import org.testng.annotations.Test;

import java.net.URL;

public class DatabaseIntegrationTest extends IntegrationTest {

    @Override
    protected String getServiceRoot() {
        return "/db/";
    }

    @Test
    @RunAsClient
    public void executeStartupScripts() throws Exception {
        URL postUrl = new URL(serverUrl, APP_PATH + "/test/scripts");
        String names = "classpath:META-INF/config/schema.sql";
        template.postForObject(postUrl.toURI(), names, Void.class);
    }

}
