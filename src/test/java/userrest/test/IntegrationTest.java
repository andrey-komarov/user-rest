package userrest.test;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.container.test.api.RunAsClient;
import org.jboss.arquillian.test.api.ArquillianResource;
import org.jboss.arquillian.testng.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.springframework.web.client.RestTemplate;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import userrest.domain.UserObject;
import userrest.domain.UserObjectMap;
import userrest.mvc.UserObjectController;
import userrest.repository.DefaultMemoryUserObjectRepository;
import userrest.service.DefaultUserObjectService;
import userrest.testhelper.HelperController;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;

public abstract class IntegrationTest extends Arquillian {

    public static final String WEB_APP_SRC = "src/main/webapp";
    public static final String WEB_APP_INF_SRC = WEB_APP_SRC + "/WEB-INF";

    public static final String APP_NAME = "user-rest";
    public static final String APP_PATH = "/" + APP_NAME;

    protected abstract String getServiceRoot();

    protected static RestTemplate template;

    @ArquillianResource
    protected URL serverUrl;
    private static UserObject firstForSave;
    private static UserObject secondForSave;

    @Deployment(testable = false)
    public static WebArchive getArchive() {
        WebArchive webArchive = ShrinkWrap.create(WebArchive.class, APP_NAME + ".war")
                .addPackage(IntegrationTest.class.getPackage())

                .addPackage(UserObject.class.getPackage())
                .addPackage(UserObjectController.class.getPackage())
                .addPackage(DefaultMemoryUserObjectRepository.class.getPackage())
                .addPackage(DefaultUserObjectService.class.getPackage())
                .addPackage(HelperController.class.getPackage())

                .addAsResource("log4j.properties")
                .addAsResource("META-INF/spring/datasource-tx.xml")
                .addAsResource("META-INF/config/schema.sql")

                .addAsWebInfResource(new File(WEB_APP_INF_SRC, "spring/rest-context.xml"), "spring/rest-context.xml")
                .addAsWebInfResource(new File(WEB_APP_INF_SRC, "spring/root-context.xml"), "spring/root-context.xml")
                .addAsWebInfResource(new File(WEB_APP_INF_SRC, "spring/database.properties"),
                        "spring/database.properties")

                .setWebXML(new File(WEB_APP_INF_SRC, "web.xml"))

                .addAsLibraries(Maven.resolver()
                        .loadPomFromFile("pom.xml")
                        .importRuntimeDependencies()
                        .resolve()
                        .withTransitivity()
                        .asFile());

        System.out.println(webArchive.toString(true));

        return webArchive;
    }

    @Test
    @RunAsClient
    public void testGetAllFromEmptyStorage() throws Exception {
        assertGetUserObjectMap();
    }

    @Test(dependsOnMethods = {})
    @RunAsClient
    public void testPutToEmptyStorage() throws Exception {
        URL postUrl = new URL(serverUrl, APP_PATH + getServiceRoot());
        template.postForObject(postUrl.toURI(), firstForSave, UserObject.class);
        UserObject expectedSaved = firstForSave.copy(1L);

        assertGetUserObjectMap(expectedSaved);
        assertGetUserObject(expectedSaved);
    }

    @Test
    @RunAsClient
    public void testPutToFilledStorage() throws Exception {
        URL postUrl = new URL(serverUrl, APP_PATH + getServiceRoot());
        template.postForObject(postUrl.toURI(), secondForSave, UserObject.class);
        UserObject expectedSaved = secondForSave.copy(2L);

        assertGetUserObjectMap(firstForSave.copy(1L), expectedSaved);
        assertGetUserObject(expectedSaved);
    }

    @Test
    @RunAsClient
    public void testGetNonExistingUserObject() throws Exception {
        URL getUrl = new URL(serverUrl, APP_PATH + getServiceRoot() + "-1");
        UserObject actual = template.getForObject(getUrl.toURI(), UserObject.class);
        Assert.assertNull(actual);
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
        createTemplate();
        createUserObjectsForSave();
    }

    private static void createUserObjectsForSave() {
        firstForSave = new UserObject();
        firstForSave.setName("Name1");
        firstForSave.setSurname("Surname1");
        firstForSave.setAge(10);

        secondForSave = new UserObject();
        secondForSave.setName("Name2");
        secondForSave.setSurname("Surname2");
        secondForSave.setAge(20);
    }

    private void assertGetUserObjectMap(UserObject... expectedUserObjects) throws MalformedURLException, URISyntaxException {
        URL getUrl = new URL(serverUrl, APP_PATH + getServiceRoot());
        UserObjectMap result = template.getForObject(getUrl.toURI(), UserObjectMap.class);
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getMap());
        Assert.assertEquals(result.getMap().size(), expectedUserObjects.length);
        for (UserObject expected : expectedUserObjects) {
            UserObject actual = result.getMap().get(expected.getUserId());
            assertUserObject(actual, expected);
        }
    }

    private void assertGetUserObject(UserObject expected) throws MalformedURLException, URISyntaxException {
        URL getUrl = new URL(serverUrl, APP_PATH + getServiceRoot() + expected.getUserId());
        UserObject actual = template.getForObject(getUrl.toURI(), UserObject.class);
        assertUserObject(actual, expected);
    }

    private void assertUserObject(UserObject actual, UserObject expected) {
        Assert.assertNotNull(actual);
        Assert.assertEquals(actual.getUserId(), expected.getUserId());
        Assert.assertEquals(actual.getName(), expected.getName());
        Assert.assertEquals(actual.getSurname(), expected.getSurname());
        Assert.assertEquals(actual.getAge(), expected.getAge());
    }

    private static synchronized void createTemplate() throws IOException {
        template = new RestTemplate();
    }

}
