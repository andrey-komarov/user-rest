drop table if exists user_object;

create table user_object(
  user_id long not null auto_increment,
  name varchar(255) not null,
  surname varchar(255) not null,
  age int,
  primary key (user_id)
)